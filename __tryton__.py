#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Dunning',
    'name_de_DE': 'Buchhaltung Mahnwesen',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Account Dunning (aka Reminder)
    - Provides a generic dunning process for open items
    - Produces reminders for each party with open items
    ''',
    'description_de_DE': '''Buchhaltung Mahnwesen
    - Stellt einen allgemeinen Mahnlauf für Offene Posten zur Verfügung
    - Erstellt Mahnungen für jede Partei mit Offenen Posten
    ''',
    'depends': [
        'account',
        'account_option_party_required',
    ],
    'xml': [
        'account_dunning.xml',
        'company.xml',
        'party.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}

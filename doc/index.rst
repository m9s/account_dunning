.. only:: test

    .. warning:: The illustrated console examples are invasive
       and creating transactional data which can not be deleted.
       The console examples should not be executed in production
       environments.


===============
Account Dunning
===============


This is the documentation of functionality and usage of the
account_dunning module.
This module provides management of dunnings and remainders.
At the beginning of dunning run is the *dunning proposal* wizard, which
creates remainders for unpaid open items.

The examples in this document are based on a customer party with open items.
As a general rule the date of today is used as the base date to
create and open items.


Open Items - Dunnings
=====================

The unpaid and unreconciled receivables of a company are called in
general open items, when they are in state 'posted'.
A given payment term defines at least, after which period of time an
open item should be in due state. When an undue open item becomes overdue,
the dunning run begins. Usually the company would like to write a note to
the customer to remind his late payments. And write another note and another.
When it becomes clear, that the customer will not pay after some remainders,
the company may decide to bring the case to court.


Modul Setup
===========

Requirements:

    * A tryton client connected to a tryton server.

.. only:: test

    Console example::

        >>> ### General Setup
        >>>
        >>> import time
        >>> import datetime
        >>> from dateutil.relativedelta import relativedelta
        >>> from decimal import Decimal
        >>> from proteus import config, Model, Wizard
        >>> today = datetime.date.today()
        >>>
        >>> ### Connection
        >>>
        >>> URL = 'https://admin:admin@localhost:8069/testdb'
        >>> config = config.set_xmlrpc(URL)

    * An installed account_dunning module with all dependencies.
    * An installed account chart containing the accounts used in the
      console examples.

.. only:: test

    Console example::
        >>> # Install/update account_dunning
        >>> Module = Model.get('ir.module.module')
        >>> modules = Module.find([
        ...         ('name', 'in', ('account_dunning')),
        ...     ])
        >>> _ = Module.button_install([x.id for x in modules], config.context)
        >>> Wizard('ir.module.module.install_upgrade').execute('start')
        >>>
        >>> ### Instanciate Models
        >>>
        >>> Account = Model.get('account.account')
        >>> Address = Model.get('party.address')
        >>> Dunning = Model.get('account.dunning')
        >>> DunningLevel = Model.get('account.dunning.level')
        >>> DunningTerm = Model.get('account.dunning.term')
        >>> Invoice = Model.get('account.invoice')
        >>> InvoiceLine = Model.get('account.invoice.line')
        >>> Journal = Model.get('account.journal')
        >>> Move = Model.get('account.move')
        >>> Party = Model.get('party.party')
        >>> PaymentTerm = Model.get('account.invoice.payment_term')
        >>> PaymentTermLine = Model.get('account.invoice.payment_term.line')

    * Full Setup of Models:

        * Company
        * Access groups
        * Chart of accounts
        * Fiscal year


Payment Term
------------

Payment terms are defined at:

    * Financial Management > Configuration > Payment Terms

A payment term can be applied to a party.

Example 1: Lookup or create a payment term with *name* `10 Days net`,
set up with a *line* of *type* `remainder` after `10` *days* net.

.. only:: test

    Console example::

        >>> payment_term = PaymentTerm.find([('name', '=', '10 Days net')])
        >>> if payment_term:
        ...     payment_term = payment_term[0]
        ... else:
        ...     payment_term = PaymentTerm(name='10 Days net')
        ...     payment_term_line = PaymentTermLine(type='remainder')
        ...     payment_term_line.days = 10
        ...     payment_term.lines.append(payment_term_line)
        ...     payment_term.save()


Dunning Term
=============

Dunning terms are defined at:

    * Financial Management > Configuration > Dunning > Dunning Terms

A dunning term can be applied to a party. A dunning term consists of
one or more *dunning levels* which are ordered sequencially.

The dunning process is controled by the *Sequence* field of each level.
Each term will be proceeded from the level with the smallest sequence
to the level with the highest sequence. The *Days Limit* field for each
level determines a trigger, after how many days after confirmation the
next level will be automated proposed.

Example 2: Lookup or create a dunning term named `10-10-10`, set up with
three levels in sequence:

    * First level *Name* `1. Remainder`, with *Limit Days* `10` days,

      *Sequence* `10`.

    * Second level *Name* `1. Remainder`, with *Limit Days* `10` days,

      *Sequence* `20`.

    * Third level *Name* `1. Remainder`, with *Limit Days* `10` days,

      *Sequence* `30`.

.. only:: test

    Console example::

        >>> dunning_term = DunningTerm.find([('name', '=', '10-10-10')])
        >>> if dunning_term:
        ...     dunning_term = dunning_term[0]
        ... else:
        ...     dunning_term = DunningTerm(name='10-10-10')
        ...     dunning_level1 = DunningLevel(name='1. Remainder')
        ...     dunning_level1.sequence = 10
        ...     dunning_level1.limit_days = 10
        ...     dunning_term.levels.append(dunning_level1)
        ...     dunning_level2 = DunningLevel(name='2. Remainder')
        ...     dunning_level2.sequence = 20
        ...     dunning_level2.limit_days = 10
        ...     dunning_term.levels.append(dunning_level2)
        ...     dunning_level3 = DunningLevel(name='Last Remainder')
        ...     dunning_level3.sequence = 30
        ...     dunning_level3.limit_days = 10
        ...     dunning_term.levels.append(dunning_level3)
        ...     dunning_term.save()

.. important:: Every phase in the lifetime of a dunning is represented and
               controlled by its actual term and level.


Dunning
=======

The meaning of the term *dunning* is used as below. A dunning is:

    * a collection/overview of open items, enveloped in dunning lines
    * for a date
    * for one and only one party.
    * has an actual level

        *  is the highst level of all open items in dunning lines.

    * has references to last and next level.


A dunning has the states:

    * Proposed (only a proposition and takes no effect)
    * Confirmed (is 'armed' and shown to the customer in a report)
    * Closed (has a dunning line with a child)
    * Canceled (is canceled and takes no effect)


Dunning Line
============

The meaning of the term *dunning line* is used as below. A dunning line:

    * is an envelope for an overdue unpaid receivable move line
    * is in a parent - child relationship with other dunning lines

A dunning line has a level, which is determined by the parties dunning term
and the level of a feasible parent dunning line.

A dunning line has the states:

    * Proposed
    * Confirmed
    * Closed - Superseeded (is closed, but has a child)
    * Canceled


Create an Open Item for a Customer
==================================

An open item can be created with an out invoice to a customer.

Example 3: Create a new customer party, with the above payment term.

.. only:: test

    Console example::

        >>> customer = Party(name='Customer %i' % (int(time.time()),))
        >>> customer.payment_term = payment_term
        >>> customer_address = Address(city="Lunar City")
        >>> customer.addresses[0] = customer_address
        >>> customer.save()


Example 4: Create an out-invoice with the given payment term from above.
The total amount of the invoice is 100 EUR. The invoice state is draft.

.. only:: test

    Console example::

        >>> account_revenue, = Account.find([
        ...         ('kind', '=', 'revenue'),
        ...         ('fiscalyear.code', '=', today.year),
        ...         [ 'OR', [('name', '=', 'Main Revenue')],
        ...                 [('code', '=', '4400')],
        ...         ]])
        ...
        >>> invoice = Invoice()
        >>> invoice.party = customer
        >>> invoice_line = InvoiceLine(description='Cake')
        >>> invoice_line.unit_price = Decimal('100.00')
        >>> invoice_line.account = account_revenue
        >>> invoice_line.quantity = 1.0
        >>> invoice.lines.append(invoice_line)
        >>> invoice.save()
        >>> invoice.reload()


Dunning Run
===========

The general procedure of a dunning run is shown in the following list:

    1. Propose Dunnings, which automatically generates dunning proposals
    2. Manually check the dunning proposals with the actions:

       - Remove unused dunning proposals
       - Remove unnused dunning lines
       - Blocking certain dunning lines

    3. Confirm Dunnings
    4. Print dunnings and send them to the customer.


Propose Dunnings
================


The Propose Dunnings wizard can be found in the menu:

    * Financial Management > Dunning Run > Propose Dunnings

The Propose Dunnings view is a wizard, which helps to suggest possible
dunnings. It is temporal controlled by the date field. This field indicates
the date for the proposals. The action Propose Dunnings creates the dunning
proposals for the given settings.

The Dunning Proposals widget is a list with limited editing possiblities.
The user can remove dunnings from the Dunning Proposals list and edit
existing dunning proposals. But it is not allowed to manual create dunning
proposals.


.. warning:: To use the dunning proposal action, it is needed that all move lines
    including a receivable account, have a given party. Tryton will raise an
    error, if it finds receivable move lines without a party!

.. important:: Overdue open items will only shown, when their parties have set a
    dunning term.

    .. only:: test

        Console example::

            >>> valuation_date = today + datetime.timedelta(days=20)
            >>> propose_dunnings = Wizard('wizard.propose.dunnings')
            >>> propose_dunnings.form.valuation_date = valuation_date
            >>> propose_dunnings.execute('propose')
            >>> proposed_dunnings = Dunning.find([
            ...         ('state', '=', 'proposed'),
            ...         ('party', '=', customer.id)])
            >>> len(proposed_dunnings)
            0
            >>> ### Add dunning term to party
            >>> customer.dunning_term = dunning_term
            >>> customer.save()

.. important:: Open items in state *draft* are not proposed.

    .. only:: test

        Console example::
            >>> valuation_date = today + datetime.timedelta(days=20)
            >>> propose_dunnings = Wizard('wizard.propose.dunnings')
            >>> propose_dunnings.form.valuation_date = valuation_date
            >>> propose_dunnings.execute('propose')
            >>> proposed_dunnings = Dunning.find([
            ...         ('state', '=', 'proposed'),
            ...         ('party', '=', customer.id)])
            >>> len(proposed_dunnings)
            0

            >>> ### Open invoice
            >>> invoice.invoice_date = today
            >>> _ = Invoice.workflow_trigger_validate(invoice.id, 'open',
            ...         config.context)
            >>> invoice.reload()
            >>> ### Check propose dunnings
            >>> valuation_date = today + datetime.timedelta(days=20)
            >>> propose_dunnings = Wizard('wizard.propose.dunnings')
            >>> propose_dunnings.form.valuation_date = valuation_date
            >>> propose_dunnings.execute('propose')
            >>> proposed_dunnings = Dunning.find([
            ...         ('state', '=', 'proposed'),
            ...         ('party', '=', customer.id)])
            >>> len(proposed_dunnings)
            1
            >>> proposed_dunnings[0].lines[0].overdue
            10


Proposing the dunnings 1000 days later from today from today, will result in
a similar proposal with another overdue value on the proposed dunning line.

.. important:: The valuation_date has no influence of the proposed dunning
    level. The level is only depending of a preceeding level. Without a
    preceeding level, the level with the smallest *sequence* is choosen.

    .. only:: test

        Console example::

            >>> valuation_date = today + datetime.timedelta(days=1000)
            >>> propose_dunnings = Wizard('wizard.propose.dunnings')
            >>> propose_dunnings.form.valuation_date = valuation_date
            >>> propose_dunnings.execute('propose')
            >>> proposed_dunnings = Dunning.find([
            ...         ('state', '=', 'proposed'),
            ...         ('party', '=', customer.id)])
            >>> len(proposed_dunnings)
            1
            >>> proposed_dunnings[0].lines[0].overdue
            990

Proposing dunnings for a valuation date of today, will not result any
open items of the customer party. This is because the open item is as
long undue as the given days in payment term plus the limit days of first or
actual dunning level.

.. only:: test

    Console example::

        >>> valuation_date = today
        >>> propose_dunnings = Wizard('wizard.propose.dunnings')
        >>> propose_dunnings.form.valuation_date = valuation_date
        >>> propose_dunnings.execute('propose')
        >>> proposed_dunnings = Dunning.find([
        ...         ('state', '=', 'proposed'),
        ...         ('party', '=', customer.id)])
        >>> len(proposed_dunnings)
        0

Proposing dunnings for a valuation date of 19 days later from today,
will result in an empty list, because the dunning terms first level has
limit days  of 10 days plus 10 days for the payment term. So it takes at
least 20 days to propose the dunning for first overdue open item.
19 days is one day to early to propose the open item as a dunning.
20 days exactly fit for dunning proposal.

.. only:: test

    Console example::

        >>> valuation_date = today + datetime.timedelta(days=19)
        >>> propose_dunnings = Wizard('wizard.propose.dunnings')
        >>> propose_dunnings.form.valuation_date = valuation_date
        >>> propose_dunnings.execute('propose')
        >>> proposed_dunnings = Dunning.find([
        ...         ('state', '=', 'proposed'),
        ...         ('party', '=', customer.id)])
        >>> len(proposed_dunnings)
        0
        >>> valuation_date = today + datetime.timedelta(days=20)
        >>> propose_dunnings = Wizard('wizard.propose.dunnings')
        >>> propose_dunnings.form.valuation_date = valuation_date
        >>> propose_dunnings.execute('propose')
        >>> proposed_dunnings = Dunning.find([
        ...         ('state', '=', 'proposed'),
        ...         ('party', '=', customer.id)])
        >>> len(proposed_dunnings)
        1

The total of a proposed dunning after 20 days is 100.00.
The overdue is 10 days.

.. only:: test

    Console example::


        >>> proposed_dunnings[0].total
        Decimal('100.00')
        >>> proposed_dunnings[0].lines[0].overdue
        10


Remove dunning proposals
------------------------

Removing dunning proposals from the list has the consequence, that all
open items for a party will not remindered at this time. Deleted dunning
proposal will be suggested next time when the Propose Dunnings action
is called.

    .. only:: test

        Console example::

            >>> del proposed_dunnings
            >>> valuation_date = today + datetime.timedelta(days=1000)
            >>> propose_dunnings = Wizard('wizard.propose.dunnings')
            >>> propose_dunnings.form.valuation_date = valuation_date
            >>> propose_dunnings.execute('propose')
            >>> proposed_dunnings = Dunning.find([
            ...         ('state', '=', 'proposed'),
            ...         ('party', '=', customer.id)])
            >>> len(proposed_dunnings)
            1

.. important:: The below 'confirm' action will be applied on all dunning
    proposals in the list. A long list needs time to process 'confirm'.
    In this case it is appropriate to 'confirm' packets of dunnings in
    sequence.


Edit dunning proposals
----------------------

Dunning proposals can be edited before confirmation.
It is possible to remove or block dunning lines from a proposal.

    * Removing a dunning line from a proposal has the consequence that the
      level will be reset to the first possible level of the term.
    * Blocking a dunning line from a proposal will 'freeze' the proposed level
      and set zero amount. A blocked dunning line will not shown in the report.


Other influences to dunning proposals
-------------------------------------

If the customer pays the open item, and the accountant creates the
appropriate move for the payment, the dunning line will not shown
again.

If an open item will not or never paied by the customer, the
accountant can create a move to balance the open item from the default loss
account.


Example 7: The customer pays today 25 EUR. It is a partial payment of the open
item (100 EUR) from the invoice.

.. only:: test

    Console example::

        >>> pay_invoice = Wizard('account.invoice.pay_invoice', [invoice])
        >>> pay_invoice.form.amount = Decimal('25.0')
        >>> pay_invoice.form.journal = Journal.find([('type', '=', 'cash')])[0]
        >>> pay_invoice.execute('choice')
        >>> pay_invoice.execute('pay')
        >>> invoice.reload()
        >>> invoice.amount_to_pay
        Decimal('75.00')
        >>> Move.button_post([invoice.payment_lines[0].move.id], config.context)
        False

When propose dunnings 20 days after today, a dunning proposal will be created.
The amount to pay is 75 EUR, because 25 EUR were paid just before a moment.
Undue is 10 days. Payment is 25 EUR.

.. only:: test

    Console example::

        >>> valuation_date = today + datetime.timedelta(days=20)
        >>> propose_dunnings = Wizard('wizard.propose.dunnings')
        >>> propose_dunnings.form.valuation_date = valuation_date
        >>> propose_dunnings.execute('propose')
        >>> proposed_dunnings = Dunning.find([
        ...         ('state', '=', 'proposed'),
        ...         ('party', '=', customer.id)])
        >>> len(proposed_dunnings)
        1
        >>> proposed_dunnings[0].total
        Decimal('75.00')


Confirm Dunning Proposals
-------------------------

Only dunning proposals with a level can be confirmed. If proposals without
a level are shown, it indicates, that the party of the dunning proposal has
no valid dunning term.

After confirmation of a dunning proposal, a new tab will be opened with a
list of all confirmed dunnings. The confirmed dunning disappears from the
propose dunnings list.


Confirmed Dunnings
==================

Confirmed dunnings are dunnings, the user is gouing to inform to the customer.
They are also the base of the next call of the propose dunnings action. A
confirmed dunning report is considered as sended to the overdue customer.
Which means, the customer get knowledge about a confirmed dunning.

An accidentally confirmed dunning can be canceled, too.
The cancelation of a dunning has some effects:

   * A canceled dunning is not deleted,
   * it get the state *canceled*
   * the parent dunning and all of its lines are reset from state *open* to
     *confirmed*.

All confirmed dunnings can be shown by using this menu path:

   * Financial Management > Dunning Run > Confirmed Dunnings

When a proposed dunning is confirmed the first time, it becomes the state
*confirmed* ad the level name '1. Remainder'.

     .. only:: test

         Console example::

             >>> propose_dunnings.execute('init_confirm')
             >>> proposed_dunnings[0].reload()
             >>> proposed_dunnings[0].state
             'confirmed'
             >>> proposed_dunnings[0].level.name
             '1. Remainder'

.. note:: Next time when proposing dunnings, the dunning for the open item
    will be proposed with level 2. The trigger date is 30 days after the
    invoice has opened. 10 days of the payment term days net *plus* 10 days
    from the limit days of dunning level one *plus* 10 days from level two.

    .. only:: test

        Console example::

            >>> valuation_date = today + datetime.timedelta(days=30)
            >>> propose_dunnings = Wizard('wizard.propose.dunnings')
            >>> propose_dunnings.form.valuation_date = valuation_date
            >>> propose_dunnings.execute('propose')
            >>> proposed_dunnings = Dunning.find([
            ...         ('state', '=', 'proposed'),
            ...         ('party', '=', customer.id)])
            >>> len(proposed_dunnings)
            1
            >>> proposed_dunnings[0].state
            'proposed'
            >>> proposed_dunnings[0].level.sequence
            20

Example 8: Next time using propose dunnings, the proposed level depends
of the last level of the preceding dunning.
Last dunning was confirmed with Level 1. Next level in dunning term
sequence is level 2. The limit days of level 2 is 10.

.. only:: test

    Console example::

        >>> valuation_date = today + datetime.timedelta(days=30)
        >>> propose_dunnings = Wizard('wizard.propose.dunnings')
        >>> propose_dunnings.form.valuation_date = valuation_date
        >>> propose_dunnings.execute('propose')
        >>> proposed_dunnings = Dunning.find([
        ...         ('state', '=', 'proposed'),
        ...         ('party', '=', customer.id)])
        >>> len(proposed_dunnings)
        1
        >>> # Missing test:confirm dunning
        >>> propose_dunnings.execute('init_confirm')
        >>> proposed_dunnings[0].reload()
        >>> proposed_dunnings[0].state
        'confirmed'
        >>> proposed_dunnings[0].level.name
        '2. Remainder'

        >>> valuation_date = today + datetime.timedelta(days=40)
        >>> propose_dunnings = Wizard('wizard.propose.dunnings')
        >>> propose_dunnings.form.valuation_date = valuation_date
        >>> propose_dunnings.execute('propose')
        >>> proposed_dunnings = Dunning.find([
        ...         ('state', '=', 'proposed'),
        ...         ('party', '=', customer.id)])
        >>> len(proposed_dunnings)
        1
        >>> proposed_dunnings[0].level.sequence
        30
        >>> propose_dunnings.execute('init_confirm')
        >>> proposed_dunnings[0].reload()
        >>> proposed_dunnings[0].state
        'confirmed'
        >>> proposed_dunnings[0].level.name
        'Last Remainder'


.. information:: When the last level of a dunning term is reached, this
    level will be repeated as long as the open item is closed by a payment
    or write off.

    .. only:: test

        Console example::

            >>> valuation_date = today + datetime.timedelta(days=50)
            >>> propose_dunnings = Wizard('wizard.propose.dunnings')
            >>> propose_dunnings.form.valuation_date = valuation_date
            >>> propose_dunnings.execute('propose')
            >>> proposed_dunnings = Dunning.find([
            ...         ('state', '=', 'proposed'),
            ...         ('party', '=', customer.id)])
            >>> len(proposed_dunnings)
            1
            >>> proposed_dunnings[0].state
            'proposed'
            >>> proposed_dunnings[0].level.name
            'Last Remainder'


Closed Dunnings
===============

Closing a dunning happens indirectly by the confirmation of a child dunning.
A confirmed dunning will be closed, when a proposed child dunning is
confirmed. Closed dunnings are nearly immutable, since there are other
confirmed child dunnings depending from the informations of the closed
dunning.

Closed dunnings can be shown by using this menu path:

    * Financial Management > Dunning Run > Closed Dunnings


Cancel Dunnings
===============

Canceled dunnigs are former confirmed dunnings, which are canceled.
These dunnings do not have an effect to the system and are for
informational use. In a canceled dunning, all dunning lines are canceled,
too.

All canceled dunnings can be shown by using this menu path:

    * Financial Management > Dunning Run > Canceled Dunnings



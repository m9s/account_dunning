#This file is part of Tryton.  The COPYRIGHT file at the top level
#of this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields

class Party(ModelSQL, ModelView):
    _name = 'party.party'

    dunning_term = fields.Property(fields.Many2One('account.dunning.term',
            'Term', help='The dunning term controls the dunning run for a party.',
            select=1))

Party()

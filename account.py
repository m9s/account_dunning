# This file is part of Tryton.  The COPYRIGHT file at the top level
# of this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields

class Line(ModelSQL, ModelView):
    _name = 'account.move.line'

    dunning_lines = fields.One2Many('account.dunning.line', 'move_line',
        'Dunning Lines', readonly=True)

Line()

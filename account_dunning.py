#This file is part of Tryton.  The COPYRIGHT file at the top level
#of this repository contains the full copyright notices and license terms.

#TODO Check if another state 'running' between confirmed and closed is needed.

'Dunning'
import datetime
import copy
import base64
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.report import Report
from trytond.wizard import Wizard
from trytond.backend import TableHandler
from trytond.pyson import Equal, Eval, Not, In, Bool, Or
from trytond.transaction import Transaction
from trytond.pool import Pool


STATES = {
    'readonly': Not(Equal(Eval('state'), 'proposed')),
}
STATES_DEPENDS = ['state']
ZERO = Decimal('0.0')


class DunningTerm(ModelSQL, ModelView):
    'Dunning Term'
    _name = 'account.dunning.term'
    _description = __doc__

    name = fields.Char('Name', required=True, translate=True, loading='lazy')
    party = fields.One2Many('party.party', 'dunning_term', 'Party')
    levels = fields.One2Many('account.dunning.level', 'term', 'Levels',
            required=True, help='The successive dunning levels control the '
            'process of the dunning run in detail.')
    limit_amount = fields.Numeric('Limit Amount',
            digits=(16, Eval('currency_digits', 2)), depends=['currency_digits'],
            help='Only create dunnings for '
            'amounts bigger than the limit amount. If the limit amount '
            'is zero, then dunnings will be created anyway.')
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
            'get_currency_digits')
    has_undue = fields.Boolean('Include undue', help='Generally include open '
            'items which are undue.\nNote: If there are payments on an open '
            'item, all undue open items will shown on the dunning anyway.')

    active = fields.Boolean('Active', select=2)

    def __init__(self):
        super(DunningTerm, self).__init__()
        self._order.insert(1, ('name', 'ASC'))

    def default_currency_digits(self):
        return 2

    def default_has_undue(self):
        return False

    def default_active(self):
        return True

    def get_currency_digits(self, ids, names):
        res = {}
        for line in self.browse(ids):
            for name in names:
                res.setdefault(name, {})
                res[name].setdefault(line.id, 2)
        return res

    def get_first_level(self, term_id=None):
        level_obj = Pool().get('account.dunning.level')
        if not term_id:
            return
        first_level = None
        first_level_id = level_obj.search([
                        ('term', '=', term_id),
                ], limit=1, order=[('sequence', 'ASC')])
        if first_level_id:
            first_level = level_obj.browse(first_level_id)[0]

        return first_level

    def get_next_level(self, term_id=None, level_id=None):
        level_obj = Pool().get('account.dunning.level')
        if not term_id:
            return
        if not level_id:
            return self.get_first_level()
        next_level = None
        level = level_obj.browse(level_id)
        next_level_id = level_obj.search([
                ('term', '=', term_id),
                ('sequence', '>', level.sequence),
                ], limit=1, order=[('sequence', 'ASC')])
        if next_level_id:
            next_level = level_obj.browse(next_level_id)[0]
        return next_level

    def get_last_level(self, term_id=None):
        level_obj = Pool().get('account.dunning.level')
        if not term_id:
            return
        last_level = None
        last_level_id = level_obj.search([
                ('term', '=', term_id),
                ], limit=1, order=[('sequence', 'DESC')])
        if last_level_id:
            last_level = level_obj.browse(last_level_id)[0]

        return last_level
DunningTerm()


class DunningLevel(ModelSQL, ModelView):
    'Dunning Level'
    _name = 'account.dunning.level'
    _description = __doc__

    sequence = fields.Integer('Sequence', required=True, help='The sequence '
            'controls the successive application of levels in a dunning run.')
    name = fields.Char('Name', required=True)
    report_name = fields.Char('Report Name', translate=True, loading='lazy',
            help='The name used in reports.')
    term = fields.Many2One('account.dunning.term', 'levels',
            select=1, ondelete="RESTRICT", required=True)
    limit_days = fields.Integer('Days Limit', help='Count of days until '
            'changing to this level from a predessing level.')
    dunning_line = fields.One2Many('account.dunning.line', 'level',
            'Dunning Lines')
    report_text = fields.Text('Report Text', translate=True, loading='lazy',
            help='The report text can be used in the dunning report, to have '
            'different paragraphs for each level.')

    def init(self, module_name):
        super(DunningLevel, self).init(module_name)
        cursor = Transaction().cursor
        table = TableHandler(cursor, self, module_name)

        # REMOVE ME on Release?
        # Migration from last devel drop sequence_uniq constraint
        table.drop_constraint('sequence_uniq')

    def __init__(self):
        super(DunningLevel, self).__init__()
        self._sql_constraints += [
            ('term_sequence_uniq', 'UNIQUE(term, sequence)',
                'Sequence of each dunning term must be unique!'),
        ]
        self._order.insert(0, ('sequence', 'ASC'))

    def default_sequence(self):
        return 10

DunningLevel()


class Dunning(ModelSQL, ModelView):
    'Dunning'
    _name = 'account.dunning'
    _description = __doc__
    _rec_name = 'date'

    date = fields.Date('Date', help='Date of the dunning creation.',
            readonly=True, states={
                'required': Not(Equal(Eval('state'), 'proposed')),
            }, depends=['state'])
    valuation_date = fields.Date('Valuation Date', help='Date of the '
            'valuation of the dunning.', readonly=True, required=True)
    lines = fields.One2Many('account.dunning.line', 'dunning',
            'Dunning Lines', states=STATES, depends=STATES_DEPENDS, on_change=[
                'payments', 'lines', 'amount_payable', 'total', 'level',
            ], help='The open items of the party of this '
            'dunning. Undue open items are colored grey.')
    payments = fields.One2Many('account.dunning.payment.line', 'dunning',
            'Payment Lines at Valuation Date', on_change=['payments', 'lines',
                'amount_paid', 'total'],
            help='The payments of the party of this dunning at the ' \
                    'valuation date')
    party = fields.Many2One('party.party', 'Party', change_default=True,
            required=True, help='The party which will '
            'be chased for payment with this dunning.', readonly=True)
    address = fields.Many2One('party.address', 'Address', required=True,
            domain=[('party', '=', Eval('party'))], states={
                'readonly': Not(Equal(Eval('state'), 'proposed')),
            }, depends=['party', 'state'],
            help='The address where this dunning will send.')
    amount_payable = fields.Function(fields.Numeric('Amount Payable',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits', 'lines'],
            help='The amount to pay for this dunning'), 'get_amount_payable')
    amount_paid = fields.Function(fields.Numeric('Amount Paid',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits', 'payments'],
            help='The amount paid for this dunning'), 'get_amount_paid')
    total = fields.Function(fields.Numeric('Total',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits', 'amount_payable', 'amount_paid'],
            help='The total amount to pay for this dunning'), 'get_total')
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
            'get_currency_digits')
    level = fields.Function(fields.Many2One('account.dunning.level', 'Level',
            readonly=True, states={
                'required': Not(Equal(Eval('state'), 'proposed')),
            }, depends=['state'],
            help='The level of this dunning. The level is automatically '
            'built from the highest level of the dunning lines below.'),
            'get_level')
    state = fields.Selection([
            ('proposed', 'Proposed'),
            ('confirmed', 'Confirmed'),
            ('canceled', 'Canceled'),
            ('closed', 'Closed')], 'State', readonly=True, select=1)
    company = fields.Many2One('company.company', 'Company', required=True)
    dunning_report_cache = fields.Binary('Dunning Report', readonly=True)
    dunning_report_format = fields.Char('Dunning Report Format', readonly=True)

    def __init__(self):
        super(Dunning, self).__init__()
        self.proposal_list = []
        self._error_messages.update({
            'move_line_party_missing': 'Missing party on move line "%s" '
            'at move "%s"!\n\nTryton will not propose dunning lines, if '
            'move lines without a party are involved!',
            'negative_total':'It is not allowed to save a dunning with zero '
            'or negative total!',
            'empty_dunning_lines':'It is not allowed to save a dunning '
            'without dunning lines!',})
        self._order.insert(0, ('state', 'ASC'))
        self._order.insert(1, ('party', 'ASC'))
        self._order.insert(3, ('date', 'ASC'))

    def init(self, module_name):
        super(Dunning, self).init(module_name)
        cursor = Transaction().cursor
        table = TableHandler(cursor, self, module_name)

        # Migration from 2.0 dunning_report renamed into dunning_report_cache
        # to remove base64 encoding

        if (table.column_exist('dunning_report')
                and table.column_exist('dunning_report_cache')):
            cursor.execute('SELECT id, dunning_report '
                'FROM "' + self._table + '"')
            for dunning_report_id, report in cursor.fetchall():
                if report:
                    report = buffer(base64.decodestring(str(report)))
                    cursor.execute('UPDATE "' + self._table + '" '
                        'SET dunning_report_cache = %s '
                        'WHERE id = %s', (report, dunning_report_id))
                table.drop_column('dunning_report')

    def default_currency_digits(self):
        return 2

    def default_state(self):
        return 'proposed'

    def default_company(self):
        return Transaction().context.get('company') or False

    def on_change_lines(self, vals):
        return self._on_change_lines_payments(vals)

    def on_change_payments(self, vals):
        return self._on_change_lines_payments(vals)

    def _on_change_lines_payments(self, vals):
        dunning_lines = vals.get('lines', [])
        payment_lines = vals.get('payments', [])
        res = {
            'amount_paid': ZERO,
            'amount_payable': ZERO,
            'total': ZERO,
        }

        for dunning_line in dunning_lines:
            if not dunning_line.get('block'):
                res['amount_payable'] += dunning_line.get('amount', ZERO)

        if res['amount_payable'] > ZERO:
            for payment_line in payment_lines:
                res['amount_paid'] -= payment_line.get('amount', ZERO)

        res['total'] = res['amount_payable'] + res['amount_paid']
        return res

    def get_amount_payable(self, ids, name):
        currency_obj = Pool().get('currency.currency')
        res = {}
        for dunning in self.browse(ids):
            amount = ZERO
            for open_item in dunning.lines:
                if not open_item.block:
                    amount += open_item.amount
            res[dunning.id] = currency_obj.round(dunning.company.currency,
                    amount)
        return res

    def address_get(self, party=None):
        party_obj = Pool().get('party.party')
        address = None
        if party:
            address = party_obj.address_get(party.id, type='invoice')
        if not address:
            address = party_obj.address_get(party.id)
        return address

    def get_amount_paid(self, ids, name):
        currency_obj = Pool().get('currency.currency')
        res = {}
        for dunning in self.browse(ids):
            amount = ZERO
            for payment in dunning.payments:
                amount += payment.amount
            res[dunning.id] = currency_obj.round(dunning.company.currency,
                    amount * -1)
        return res

    def get_total(self, ids, name):
        currency_obj = Pool().get('currency.currency')
        res = {}
        for dunning in self.browse(ids):
            res[dunning.id] = currency_obj.round(dunning.company.currency,
                dunning.amount_payable + dunning.amount_paid)
        return res

    def get_currency_digits(self, ids, names):
        res = {}
        for line in self.browse(ids):
            for name in names:
                res.setdefault(name, {})
                res[name].setdefault(line.id, 2)
                if name == 'currency_digits':
                    if line.lines:
                        res[name][line.id] = \
                                line.lines[0].move_line.account.currency_digits
                elif name == 'second_currency_digits':
                    if line.account.second_currency:
                        res[name][line.id] = \
                        line.lines[0].move_line.account.second_currency.digits
        return res

    def get_level(self, ids, name):
        term_obj = Pool().get('account.dunning.term')
        dunning_obj = Pool().get('account.dunning')
        dunnings = dunning_obj.browse(ids)
        res = {}
        for dunning in dunnings:
            level = None
            term = dunning.party.dunning_term
            if term:
               level = term_obj.get_first_level(term.id)
               level_id = level.id
            old_seq = 0
            res[dunning.id] = False
            lines = dunning.lines
            for line in lines:
                if 'level' in line and line.level.id:
                    if line.level.sequence > old_seq:
                        old_seq = new_seq = line.level.sequence
                        level_id = line.level.id
            res[dunning.id] = level_id
        return res

    def get_rec_name(self, ids, name):
        if not ids:
            return {}
        res = {}
        for dunning in self.browse(ids):
            if 'level' in dunning and dunning.level:
                res[dunning.id] = dunning.level.name + \
                        '[' + str(dunning.date) + ']'
            else:
                res[dunning.id] = '[' + str(dunning.date) + ']'
        return res

    def get_receivable_move_lines(self, account_ids=None, parties=None):
        move_line_obj = Pool().get('account.move.line')
        if not account_ids:
            return []
        receivable_move_line_query_list = [
                    ('state', '=', 'valid'),
                    ('move_state', '=', 'posted'),
                    ('credit', '=', 0),
                    ('debit', '>', 0),
                    ('reconciliation', '=', False),
                    ('account', 'in', account_ids),
        ]
        if parties:
            receivable_move_line_query_list.append(('party', 'in', parties))
        receivable_move_line_ids = move_line_obj.search(
                receivable_move_line_query_list,
                order=[('maturity_date', 'ASC')])
        receivable_move_lines = move_line_obj.browse(receivable_move_line_ids)
        return receivable_move_lines, receivable_move_line_ids

    def get_payment_move_lines(self, account_ids=None, parties=None):
        move_line_obj = Pool().get('account.move.line')
        if not account_ids:
            return []
        payment_move_line_query_list = [
                    ('state', '=', 'valid'),
                    ('move_state', '=', 'posted'),
                    ('credit', '>', 0),
                    ('debit', '=', 0),
                    ('reconciliation', '=', False),
                    ('account', 'in', account_ids),
        ]
        if parties:
            payment_move_line_query_list.append(('party', 'in', parties))
        payment_move_lines = move_line_obj.search_read(
                payment_move_line_query_list, order=[('maturity_date', 'ASC')],
                fields_names=['credit', 'party'])
        return payment_move_lines

    def get_proposals(self, valuation_date=None, parties=None):
        '''
        Search move lines to propose for dunning and build a proposal_list.

        :param valuation_date: the effective date for creating the proposals
        '''
        pool = Pool()
        date_obj = pool.get('ir.date')
        account_obj = pool.get('account.account')
        dunning_line_obj = pool.get('account.dunning.line')
        payment_line_obj = pool.get('account.dunning.payment.line')
        term_obj = pool.get('account.dunning.term')
        level_obj = pool.get('account.dunning.level')
        undue_lines = set([])

        # Reset class variable
        self.proposal_list = []

        if valuation_date is None:
            valuation_date = date_obj.today()

        receivable_account_ids = account_obj.search([
                        ('reconcile', '=', True),
                        ('kind', '=', 'receivable'),
                ])

        receivable_move_lines, receivable_move_line_ids = \
                self.get_receivable_move_lines(receivable_account_ids, parties)
        payment_move_lines =  self.get_payment_move_lines(
                receivable_account_ids, parties)

        parent_confirmed_dunning_line_ids = dunning_line_obj.search([
                        ('child', '=', False),
                        ('state', '=', 'confirmed'),
                        ('move_line', 'in', receivable_move_line_ids),
                ])
        parent_confirmed_dunning_lines = dunning_line_obj.browse(
                parent_confirmed_dunning_line_ids)

        for move_line in receivable_move_lines:
            is_early = False
            parent_id = None
            is_blocked = False

            if not move_line.party:
                self.raise_user_error('move_line_party_missing', error_args=(
                        move_line.name, move_line.move.name))

            # Skip move_lines
            if not move_line.party.dunning_term:
                continue

            maturity_date = move_line.date
            if move_line.maturity_date:
                maturity_date = move_line.maturity_date

            if maturity_date >= valuation_date:
                undue_lines.add(move_line)
                continue

            term = move_line.party.dunning_term
            first_level = term_obj.get_first_level(term.id)
            proposed_level_id = first_level.id

            # Check parent confirmed dunning lines
            # TODO faster with search call?
            for line in [i for i in parent_confirmed_dunning_lines \
                    if i.move_line.id == move_line.id]:
                next_level = term_obj.get_next_level(line.level.term.id,
                        line.level.id)
                if next_level is None:
                    next_level = line.level
                trigger_date = line.level_entry_date + \
                            datetime.timedelta(next_level.limit_days)
                # Check if it is to early to create a proposal
                if valuation_date >= trigger_date:
                    proposed_level_id = next_level.id
                    is_blocked = line.block
                    parent_id = line.id
                    if is_blocked:
                        proposed_level_id = line.level.id

                    if not next_level:
                        #last level reached: last level_id stays forever
                        proposed_level_id = line.level.id
                else:
                    is_early = True


            # Check first dunning lines
            if not parent_id:
                trigger_date = maturity_date + \
                        datetime.timedelta(first_level.limit_days)
                # Check if it is to early to create a proposal
                if valuation_date >= trigger_date:
                    proposed_level_id = first_level.id
                else:
                    is_early = True

            # if the proposal is premature, skip
            if is_early:
                continue

            self.build_dunning(valuation_date, move_line, proposed_level_id,
                    is_blocked, parent_id)
        self._add_payment_lines(payment_move_lines, valuation_date)
        self._add_undue_dunning_lines(undue_lines, valuation_date)

        return self.proposal_list

    def build_dunning(self, valuation_date=None, move_line=None,
            level_id=None, block=False, parent_id=None):
        '''
        Build method for adding new dunnings and dunning lines to dunnings.
        This method builds for each party one and only one dunning with several
        lines.
        This method checks if there is already a dunning for a given party
        (move_line.party). If not, the dunning for the party is added.
        After this, the dunning line is added to the appropriate dunning.

        :param self.proposal_list: class attribute, list of dunning proposals.
        :param valuation_date: the effective date for creating the proposals
        :param move_line: a singular move line object overdue.
        :param level_id: the proposed level for the dunning line
        :param block: Bool which indicates blocked dunning lines
        :param parent_id: the id of a parent confirmed dunning line
        '''
        state = 'proposed'
        # Append new dunning proposal
        if move_line.party.id not in [i['party'] for i in self.proposal_list]:
            address = self.address_get(move_line.party)
            self._add_dunning(valuation_date, move_line, address, state)
        idx = self.proposal_list.index([i for i in self.proposal_list \
                    if move_line.party.id == i['party']][0])

        # Append new dunning proposal line
        self._add_dunning_line(idx, valuation_date, move_line, level_id, block,
                parent_id, state)

    def _add_dunning(self, valuation_date=None, move_line=None, address=None,
            state='proposed'):
        '''
        Add a singular dunning to class variable self.proposal_list.

        :param self.proposal_list: class attribute, list of dunning proposals.
        :param valuation_date: the effective date for creating the proposals
        :param move_line: a singular move line object overdue.
        :param address: the address of the party of the dunning
        :param: state: the state of the dunning
        '''
        self.proposal_list.append({
                'valuation_date': valuation_date,
                'company': move_line.account.company.id,
                'party': move_line.party.id,
                'address': address,
                'state': state,
                'lines': [],
                'payments': [],
            })

    def _add_dunning_line(self, idx=None, valuation_date=None, move_line=None,
            level_id=None, block=False, parent_id=None, state='proposed'):
        '''
        Add a singular dunning lines to class variable self.proposal_list
        into record index idx.

        :param self.proposal_list: class attribute, list of dunning proposals.
        :param valuation_date: the effective date for creating the proposals
        :param move_line: a singular move line object overdue.
        :param level_id: the proposed level for the dunning line
        :param block: Bool which indicates blocked dunning lines
        :param parent_id: the id of a parent confirmed dunning line
        :param state: the state of the dunning line
        '''
        amount = ZERO
        overdue = 0
        if idx is None:
            return

        if level_id is None:
            state = 'undue'

        maturity_date = move_line.date
        if  move_line.maturity_date:
            maturity_date = move_line.maturity_date

        if valuation_date is not None:
            overdue = valuation_date - maturity_date

        if overdue.days >= 1:
            amount = move_line.debit

        self.proposal_list[idx]['lines'].append(
            tuple(['create', {
                'move_line': move_line.id,
                'amount': amount,
                'state': state,
                'date_to_pay': maturity_date,
                'overdue': overdue.days,
                'block': block,
                'parent': parent_id,
                'level': level_id or False,
                'level_entry_date': level_id and valuation_date \
                                    or False,
            }]))

    def _add_payment_lines(self, payment_move_lines=None, valuation_date=None):
        '''
        Add payment lines to class variable self.proposal_list.

        :param self.proposal_list: class attribute, list of dunning proposals.
        :param payment_move_lines: a list of move lines lines paid.
        :param valuation_date: the effective date for creating the proposals

        '''
        # Append all payment lines
        for payment_line in payment_move_lines:
            for idx, proposal in enumerate(self.proposal_list):
                if payment_line['party'] == proposal['party']:
                    self.proposal_list[idx]['payments'].append(
                        tuple(['create', {
                            'move_line': payment_line['id'],
                            'date': valuation_date,
                            'amount': payment_line['credit'],
                        }]))

    def _add_undue_dunning_lines(self, undue_lines=None, valuation_date=None):
        '''
        Add undue lines to dunning proposals. If dunning term has_undue is
        True, or if there are payment lines in a dunning,
        undue lines will be added.

        :param self.proposal_list: class attribute, list of dunning proposals.
        :param undue_lines: a list of undue lines.
        :param valuation_date: the effective date for creating the proposals

        '''
        # append all undue dunning proposal lines
        parties_with_payments = \
                [i['party'] for i in self.proposal_list if i['payments']]
        for line in undue_lines:
            for idx, undue in enumerate(self.proposal_list):
                if line.party.id == undue['party']:
                    if line.party.dunning_term.has_undue \
                            or line.party.id in parties_with_payments:
                        maturity_date = line.date
                        if line.maturity_date:
                            maturity_date = line.maturity_date

                        overdue = valuation_date - maturity_date
                        self.proposal_list[idx]['lines'].append(
                            tuple(['create', {
                                'move_line': line.id,
                                'date_to_pay': maturity_date,
                                'state': 'undue',
                                'overdue': overdue.days,
                            }]))

    def create_proposals(self, proposal_list=None):
        '''
        Create dunning proposals from proposal_list.

        :param proposal_list: list of dunning proposals
        '''
        if not proposal_list:
            return

        for dunning in proposal_list:
            with Transaction().set_user(0):
                self.create(dunning)

    def print_report(self, dunning_id):
        '''
        Execute dunning report and store it in dunning_report field.

        :param dunning_id: the id of the dunning to execute
        '''
        dunning_report = Pool().get('account.dunning', type='report')
        val = dunning_report.execute([dunning_id], {'id': dunning_id})
        with Transaction().set_user(0):
            self.write(dunning_id, {
                    'dunning_report_format': val[0],
                    'dunning_report_cache': val[1],
                    })

    def confirm_dunning(self, ids):
        date_obj = Pool().get('ir.date')
        dunning_line_obj = Pool().get('account.dunning.line')

        date = date_obj.today()

        self.write(ids, {
            'date': date,
            'state': 'confirmed',
            })

        args = [
            ('dunning', 'in', ids),
            ('state', '=', 'proposed'),
            ]
        dunning_line_ids = dunning_line_obj.search(args)
        dunning_line_obj.write(dunning_line_ids, {'state': 'confirmed'})

        return True

    def trigger_set_state_closed(self, line_ids, trigger_id):
        dunning_line_obj = Pool().get('account.dunning.line')
        dunning_lines = dunning_line_obj.search_read([('id', 'in', line_ids)],
            fields_names=['dunning'])
        dunning_ids = [x for x in set([x['dunning'] for x in dunning_lines])]
        dunnings_to_close = []
        for dunning_id in dunning_ids:
            confirmed_line_ids = dunning_line_obj.search([
                ('dunning', '=', dunning_id),
                ('state', 'not in', ['finished', 'superseeded', 'canceled',
                    'undue'])
                ])
            if not confirmed_line_ids:
                dunnings_to_close.append(dunning_id)
        self.write(dunnings_to_close, {'state': 'closed'})

Dunning()


class DunningLine(ModelSQL, ModelView):
    'Dunning Line'
    _name = 'account.dunning.line'
    _description = __doc__
    _rec_name = 'date_to_pay'

    overdue = fields.Integer('Days Overdue', readonly=True,
            help='Count of days between the date to pay and the dunning '
            'date.')
    date_to_pay = fields.Date('Date to Pay', readonly=True,
            help='Date of the maturity date of the open item.')
    move_line =  fields.Many2One('account.move.line', 'Move Line',
            required=True, readonly=True, domain=[
                ('move_state', '=', 'posted'),
            ], depends=['move_state'], help='Move line of the open item.')
    dunning = fields.Many2One('account.dunning', 'Dunning', select=1,
            ondelete="CASCADE", required=True)
    block = fields.Boolean('Block', states={
                'readonly': Not(Equal(Eval('state'), "proposed")),
            }, depends=['state'],
            help='For a blocked dunning line, the party is not to be chased '
            'for payment. Blocked dunning lines are not shown in reports.')
    amount = fields.Function(fields.Numeric('Amount Payable',
            digits=(16, Eval('currency_digits', 2)),
            depends=['move_line', 'currency_digits', 'block', 'dunning'],
            on_change_with=['move_line', 'block', 'state', 'dunning'],
            help='The amount to pay for the dunning line'), 'get_amount')
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
            'get_currency_digits')
    level = fields.Many2One('account.dunning.level', 'Level',readonly=True,
            help='The specific dunning level of the dunning line.',
            ondelete='RESTRICT', states={
                'required': Not(In(
                    Eval('state'), ['proposed', 'undue', 'canceled']))
                }, depends=['state'])
    level_entry_date = fields.Date('Level Entry Date', help=('The date when '
            'the level changed. It is the same date, when this '
            'dunning line was confirmed.'), readonly=True, states={
                'required': Not(In(
                    Eval('state'), ['proposed', 'undue', 'canceled']))
                }, depends=['state'])
    parent = fields.Many2One('account.dunning.line', 'Parent', readonly=True,
            select=1, help='A reference to the predecessing dunning line.')
    child = fields.Function(fields.Many2One('account.dunning.line','Child',
            readonly=True, help='The successing dunning line.'),
            'get_child_field', searcher='search_child_field')
    state = fields.Selection([
                    ('proposed', 'Proposed'),
                    ('confirmed', 'Confirmed'),
                    ('finished', 'Closed - Finished'),
                    ('superseeded', 'Closed - Superseeded'),
                    ('undue', 'Undue'),
                    ('canceled', 'Canceled'),
            ], 'State', readonly=True, select=1)
    reconciled = fields.Function(fields.Boolean('Reconciled'), 'get_reconciled')
    sequence = fields.Integer('Sequence')

    def __init__(self):
        super(DunningLine, self).__init__()
        self._constraints += [
                    ('check_recursion', 'recursive_dunnings'),
            ]
        self._error_messages.update({
                    'multiple_children': 'A dunning line can not have more ' \
                             'than one child!',
                    'party_has_no_dunning_term': 'All parties must have ' \
                             'dunning terms!',
                    'recursive_dunnings': 'You can not create recursive ' \
                             'dunning lines!',
                    'operator_not_implemented': 'Search function on field ' \
                            'child is not implemented for %s operator!'
                })
        self._order.insert(0, ('sequence','ASC'))
        self._order.insert(1, ('level', 'ASC'))
        self._order.insert(2, ('overdue', 'DESC'))

    def default_currency_digits(self):
        return 2

    def default_state(self):
        return 'proposed'

    def default_block(self):
        return False

    def default_sequence(self):
        return 10

    def get_rec_name(self, ids, name):
        if not ids:
            return {}
        res = {}
        def _name(line):
            if line.id in res:
                return res[line.id]
            elif 'level' in line and line.level:
                return line.level.name+' ['+str(line.level_entry_date)+']'
            else:
                return {}
        for line in self.browse(ids):
            res[line.id] = _name(line)
        return res

    def get_amount(self, ids, name):
        currency_obj = Pool().get('currency.currency')
        res = {}
        for line in self.browse(ids):
            amount = ZERO
            amount += line.move_line.debit
            amount -= line.move_line.credit
            res[line.id] = currency_obj.round(line.dunning.company.currency,
                    amount)
        return res

    def get_currency_digits(self, ids, names):

        res = {}
        for line in self.browse(ids):
            for name in names:
                res.setdefault(name, {})
                res[name].setdefault(line.id, 2)
                if name == 'currency_digits':
                    res[name][line.id] = line.move_line.account.currency_digits
                elif name == 'second_currency_digits':
                    if line.account.second_currency:
                        res[name][line.id] = \
                                line.account.second_currency.digits
        return res

    def get_reconciled(self, ids, name):
        res = {}
        for line in self.browse(ids):
            res[line.id] = True
            if not line.move_line.reconciliation:
                res[line.id] = False
                continue
        return res

    def get_child_field(self, ids, name):
        dunning_line_obj = Pool().get('account.dunning.line')
        res = {}
        for line in self.browse(ids):
            child_ids = dunning_line_obj.search([
            ('parent', '=', line.id),
            ('state', '!=', 'canceled'),
            ])
            res[line.id] = False
            if child_ids:
                if len(child_ids) > 1:
                    self.raise_user_error('multiple_children')
                (res[line.id],) = child_ids
        return res

    def search_child_field(self, name, clause):
        operator = clause[1]
        if operator not in ['in', 'not in', '=', '!=']:
            self.raise_user_error('operator_not_implemented', error_args=(
                    operator,))

        if operator == '=':
            operator = 'in'
        elif operator == '!=':
            operator = 'not in'

        search_value = clause[2]
        if isinstance(search_value, bool):
            args = [('parent', '=', True)]
        else:
            if isinstance(search_value, (int, long)):
                search_value = [search_value]
            args = [('id', 'in', search_value)]

        parent_ids = []
        child_ids = self.search(args)
        children = self.browse(child_ids)
        for child in children:
            parent_ids.append(child.parent.id)

        if not search_value:
            args = [('id', 'not in', parent_ids)]
            parent_ids = self.search(args)

        result_clause = [('id', operator, parent_ids)]
        return result_clause

    def on_change_with_amount(self, vals):
        pool = Pool()
        currency_obj = pool.get('currency.currency')
        dunning_obj = pool.get('account.dunning')
        move_line_obj = pool.get('account.move.line')

        amount = ZERO
        dunning = dunning_obj.browse(vals['dunning'])
        move_line = move_line_obj.browse(vals['move_line'])
        if 'block' in vals and not vals['block']:
            amount += move_line.debit
            amount -= move_line.credit
        return currency_obj.round(dunning.company.currency, amount)

    def trigger_set_state_superseeded(self, ids, trigger_id):
        lines = self.read(ids, fields_names=['parent'])
        parent_ids = set([])
        for line in lines:
            if line['parent']:
                parent_ids.add(line['parent'])
        self.write([x for x in parent_ids], {'state': 'superseeded'})

    def trigger_set_state_finished(self, move_line_ids, trigger_id):
        move_line_obj = Pool().get('account.move.line')
        move_lines = move_line_obj.read(move_line_ids,
            fields_names=['dunning_lines'])
        dunning_line_ids = []
        for line in move_lines:
            if line['dunning_lines']:
                dunning_line_ids.extend(line['dunning_lines'])
        lines_to_finish_ids = self.search([
            ('id', 'in', dunning_line_ids),
            ('state', '=', 'confirmed'),
            ])
        self.write(lines_to_finish_ids, {'state': 'finished'})

    def trigger_set_state_canceled(self, ids, trigger_id):
        dunning_obj = Pool().get('account.dunning')

        line_ids = self.search([('dunning', 'in', ids)])
        lines = self.read(line_ids, fields_names=['parent'])
        parent_ids = set([])
        for line in lines:
            if line['parent']:
                parent_ids.add(line['parent'])

        self.write(line_ids, {
            'parent': False,
            'state': 'canceled',
            })

        parent_lines = self.read([x for x in parent_ids],
            fields_names=['dunning'])
        parent_dunning_ids = [line['dunning'] for line in parent_lines]

        self.write([x for x in parent_ids], {
            'state': 'confirmed',
            })
        dunning_obj.write(parent_dunning_ids, {
            'state': 'confirmed',
            })

    def check_dunning_close(self, line_id):
        dunning_obj = Pool().get('account.dunning')
        line = self.browse(line_id)
        dunning_obj.check_set_close(line.dunning)
        return True

DunningLine()


class DunningPaymentLine(ModelSQL, ModelView):
    'Payment Line'
    _name = 'account.dunning.payment.line'
    _description = __doc__

    date = fields.Function(fields.Date('Payment Date', readonly=True,
            help='Date of the payment.'), 'get_move_line_date')
    move_line =  fields.Many2One('account.move.line', 'Move Line',
            required=True, readonly=True, domain=[
                ('move_state', '=', 'posted'),
            ], help='Move line of the payment.')
    dunning = fields.Many2One('account.dunning', 'Dunning', select=1,
            ondelete="CASCADE", required=True)
    # TODO: Functionfied aus amount.
    amount = fields.Numeric('Amount Paid', readonly=True,
            digits=(16, Eval('currency_digits', 2)), on_change=['move_line'],
            depends=['move_line', 'currency_digits'],
            help='The amount of the payment.')
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
            'get_currency_digits')

    def init(self, module_name):
        super(DunningPaymentLine, self).init(module_name)
        cursor = Transaction().cursor
        table = TableHandler(cursor, self, module_name)
        table.drop_column('date', exception=True)

    def default_currency_digits(self):
        return 2

    def get_move_line_date(self, ids, name):
        res = {}
        for line in self.browse(ids):
            res[line.id] = line.move_line.date
        return res

    def get_currency_digits(self, ids, names):
        res = {}
        for line in self.browse(ids):
            for name in names:
                res.setdefault(name, {})
                res[name].setdefault(line.id, 2)
                if name == 'currency_digits':
                    res[name][line.id] = line.move_line.account.currency_digits
                elif name == 'second_currency_digits':
                    if line.account.second_currency:
                        res[name][line.id] = \
                                line.account.second_currency.digits
        return res

DunningPaymentLine()


class PrintDunningReportWarning(ModelView):
    'Print Dunning Report Warning'
    _name = 'account.dunning.print_dunning_report.warning'
    _description = __doc__

PrintDunningReportWarning()


class PrintDunningReport(Wizard):
    'Print Dunning Report'
    _name = 'account.dunning.print_dunning_report'
    states = {
        'init': {
            'result': {
                'type': 'choice',
                'next_state': '_choice',
            },
        },
        'warning': {
            'result': {
                'type': 'form',
                'object': 'account.dunning.print_dunning_report.warning',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('print', 'Print', 'tryton-ok', True),
                ],
            },
        },
        'print': {
            'actions': ['_print_init'],
            'result': {
                'type': 'print',
                'report': 'account.dunning',
                'state': 'print_next',
                'get_id_from_action': True,
            },
        },
        'print_next': {
            'actions': ['_next_id'],
            'result': {
                'type': 'choice',
                'next_state': '_print_next',
            },
        }
    }

    def _choice(self, data):
        if len(data['ids']) > 1:
            return 'warning'
        return 'print'

    def _print_init(self, data):
        res = {}
        if 'ids' in data['form']:
            res['ids'] = data['form']['ids']
        else:
            res['ids'] = data['ids']
        return res

    def _next_id(self, data):
        res = {}
        if data['form']['ids']:
            data['form']['ids'].pop(0)
        res['ids'] = data['form']['ids']
        return res

    def _print_next(self, data):
        if not data['form']['ids']:
            return 'end'
        return 'print'

PrintDunningReport()


class DunningReport(Report):
    _name = 'account.dunning'

    def execute(self, ids, datas):
        dunning_obj = Pool().get('account.dunning')

        res = super(DunningReport, self).execute(ids, datas)
        if len(ids) > 1 or datas['id'] != ids[0]:
            res = (res[0], res[1], True, res[3])
        else:
            dunning = dunning_obj.browse(ids[0])
            res = [res[0], res[1], res[2]]
            if 'name' in dunning.level and dunning.level.name:
                res.append(dunning.level.name + '-' + dunning.party.name)
            else:
                res.append(dunning.party.name)
        return res

    def _get_objects(self, ids, model, datas):
        dunning_obj = Pool().get('account.dunning')
        with Transaction().set_context(language=False):
            res = dunning_obj.browse([ids[0]])
        return res

    def parse(self, report, objects, datas, localcontext):
        user_obj = Pool().get('res.user')
        dunning_obj = Pool().get('account.dunning')

        dunning = objects[0]

        if dunning.dunning_report_cache:
            return (dunning.dunning_report_format, dunning.dunning_report_cache)

        user = user_obj.browse(Transaction().user)
        localcontext['company'] = user.company
        res = super(DunningReport, self).parse(report, objects, datas,
                localcontext)

        # If the dunning is confirmed or closed and the report not saved in
        # dunning_report_cache there was an error somewhere.
        # So we save it now in dunning_report_cache
        if dunning.state in ('confirmed', 'closed'):
            with Transaction().new_cursor() as transaction:
                dunning_obj.write(dunning.id, {
                        'dunning_report_format': res[0],
                        'dunning_report_cache': res[1],
                    })
                transaction.cursor.commit()
        return res

DunningReport()


class DunningProposeInit(ModelView):
    'Dunning Propose'
    _name = 'account.dunning.propose.init'
    _description = __doc__

    valuation_date = fields.Date('Valuation Date', required=True)
    company = fields.Many2One(
            'company.company', 'Company', required=True)
    parties = fields.Many2Many('party.party', None, None, 'Parties')

    def default_valuation_date(self):
        date_obj = Pool().get('ir.date')
        return date_obj.today()

    def default_company(self):
        return Transaction().context.get('company') or False

DunningProposeInit()


class DunningProposeReview(ModelView):
    'Dunning Propose Review'
    _name = 'account.dunning.propose.review'
    _description = __doc__

    valuation_date = fields.Date('Valuation Date', readonly=True)
    dunnings = fields.Many2Many('account.dunning',
            None, None, 'Dunnings',
            domain=[('state', '=', 'proposed')], help='Dunning proposals to '
            'confirm. Remove all dunning proposals from this list, which '
            'should not be confirmed.')
    company = fields.Many2One(
            'company.company', 'Company', readonly=True)

    def default_dunnings(self):
        dunning_obj = Pool().get('account.dunning')
        return dunning_obj.search([
            ('state', '=', 'proposed'),
            ])

DunningProposeReview()


class WizardDunningConfirmWarning(ModelView):
    'Dunning Confirm Warning'
    _name = 'account.dunning.confirm.warning'
    _description = __doc__
WizardDunningConfirmWarning()


class WizardProposeDunnings(Wizard):
    'Create Dunning Proposals'
    _name = 'wizard.propose.dunnings'
    _description = __doc__

    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'account.dunning.propose.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('propose', 'Propose', 'tryton-go-jump', True),
                    ('init_confirm', 'Confirm', 'tryton-go-next'),
                ],
            },
        },
        'propose': {
            'actions': ['_propose'],
            'result': {
                'type': 'form',
                'object': 'account.dunning.propose.review',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('init', 'Propose', 'tryton-go-jump', True),
                    ('init_confirm', 'Confirm', 'tryton-go-next'),
                ],
            },
        },
        'init_confirm': {
            'result': {
                'type': 'choice',
                'next_state': '_choice_confirm',
            },
        },
        'warning_confirm': {
            'result': {
                'type': 'form',
                'object': 'account.dunning.confirm.warning',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('init_propose', 'Back', 'tryton-go-next', True),
                ],
            },
        },
        'confirm': {
            'result': {
                'type': 'action',
                'action': '_confirm_dunnings',
                'state': 'end',
            },
        },
    }

    def _choice_propose(self, data):
        ''' Method _choice checks if the dunning propose object already has
        dunning lines. If is so, a warning raises.
        '''
        return 'propose'

    def _propose(self, data):
        dunning_obj = Pool().get('account.dunning')
        old_proposed_dunning_ids = dunning_obj.search([
                    ('state', '=', 'proposed'),
            ])

        if old_proposed_dunning_ids:
            with Transaction().set_user(0):
                dunning_obj.delete(old_proposed_dunning_ids)

        valuation_date = data['form']['valuation_date']
        if isinstance(valuation_date, datetime.datetime):
            valuation_date = valuation_date.date()
        parties = None
        if data['form'].get('parties'):
             parties = data['form']['parties'][0][1]
        proposal_list = dunning_obj.get_proposals(valuation_date, parties)
        dunning_obj.create_proposals(proposal_list)
        res = {
            'valuation_date': valuation_date,
            'company': data['form']['company']}
        return res

    def _choice_confirm(self, data):
        ''' Method _choice checks if all dunnings have a total > 0.
        If not, a warning raises.
        '''
        dunning_obj = Pool().get('account.dunning')
        if 'dunnings' in data['form']:
            add_dunning_ids = data['form']['dunnings'][0][1]
            dunnings = dunning_obj.browse(add_dunning_ids)
            for dunning in dunnings:
                if dunning.total <= ZERO:
                    return 'warning_confirm'
            return 'confirm'
        return 'init'

    def _confirm_dunnings(self, data):
        # TODO Check existing proposed dunnings, if they are full or
        # TODO partial paid. Change the state of dunning lines paid and
        # TODO: change the state of dunnings with all lines paid.
        pool = Pool()
        dunning_obj = pool.get('account.dunning')
        dunning_line_obj = pool.get('account.dunning.line')
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')

        add_dunning_ids = data['form']['dunnings'][0][1]
        dunning_obj.confirm_dunning(add_dunning_ids)

        model_data_ids = model_data_obj.search([
                ('fs_id', '=', 'act_dunning_confirmed'),
                ('module', '=', 'account_dunning'),
                ('inherit', '=', False),
                ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        return res

WizardProposeDunnings()


class DunningCancelWarning1(ModelView):
    'Dunning Cancel Warning'
    _name = 'account.dunning.cancel.warning1'
    _description = __doc__

DunningCancelWarning1()


class DunningCancelWarning2(ModelView):
    'Dunning Cancel Warning'
    _name = 'account.dunning.cancel.warning2'
    _description = __doc__

DunningCancelWarning2()


class CancelDunnings(Wizard):
    'Cancel'
    _name = 'account.dunning.cancel'
    states = {
        'init': {
            'result': {
                'type': 'choice',
                'next_state': '_choice',
            },
        },
        'warning_1': {
            'result': {
                'type': 'form',
                'object': 'account.dunning.cancel.warning1',
                'state': [
                    ('end', 'End', 'tryton-cancel'),
                ],
            },
        },
        'warning_2': {
            'result': {
                'type': 'form',
                'object': 'account.dunning.cancel.warning2',
                'state': [
                    ('end', 'No', 'tryton-cancel'),
                    ('cancel', 'Yes', 'tryton-ok', True),
                ],
            },
        },
        'cancel': {
            'result': {
                'type': 'action',
                'object':'account.dunning',
                'action': '_cancel',
                'state': 'end',
                },
            },
        }

    def _choice(self, data):
        ''' Method _choice checks if the dunning objects are confirmed.
        '''
        dunning_obj = Pool().get('account.dunning')
        for dunning in dunning_obj.browse(data['ids']):
            if dunning.state != 'confirmed':
                return 'warning_1'
        return 'warning_2'

    def _cancel(self, data):
        dunning_obj = Pool().get('account.dunning')
        dunning_line_obj = Pool().get('account.dunning.line')

        dunning_line_ids = dunning_line_obj.search([
            ('dunning', 'in', data['ids'])
            ])
        lines = dunning_line_obj.read(dunning_line_ids,
            fields_names=['parent'])
        parent_ids = set([])
        for line in lines:
            if line['parent']:
                parent_ids.add(line['parent'])
        parent_lines = dunning_line_obj.read([x for x in parent_ids],
            fields_names=['dunning'])
        parent_dunning_ids = [line['dunning'] for line in parent_lines]
        dunning_obj.write(data['ids'], {'state': 'canceled'})
        dunning_line_obj.write(dunning_line_ids, {
            'parent': False,
            'state': 'canceled',
            })
        dunning_line_obj.write([x for x in parent_ids], {
            'state': 'confirmed',
            })
        dunning_obj.write(parent_dunning_ids, {
            'state': 'confirmed',
            })
        return {}

CancelDunnings()
